package com.example.user.learningandroid;

import android.app.Dialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MainActivity extends AppCompatActivity {

    Dialog myDialog; //popup - wrong number
    private Button btn;
    EditText nameText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        myDialog = new Dialog(this);
        btn = (Button) findViewById(R.id.next1);

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TextView phoneNumber = (TextView)findViewById(R.id.phoneNumber); //text view to object
                String regexStr = "^[+]?[0-9]{10,13}$";
                if(phoneNumber.length() != 10 || phoneNumber.getText().toString().matches(regexStr)==false ){
                        ShowPopup();
                }
                else {
                    openActivity2();
                }
            }
        });
    }

    public void ShowPopup (){
        TextView txtclose;
        myDialog.setContentView(R.layout.popupwrongnum);
        txtclose = (TextView) myDialog.findViewById(R.id.popup1);
        txtclose.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                myDialog.dismiss();
            }
        });
        myDialog.show();

    }

    public void openActivity2() {
        Intent intent = new Intent(this, Activity2.class);
        startActivity(intent);
    }
}


