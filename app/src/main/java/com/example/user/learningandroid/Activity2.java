package com.example.user.learningandroid;

import android.app.Dialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class Activity2 extends AppCompatActivity {
    Dialog myDialog; //popup - wrong username
    private Button button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_2);
        myDialog = new Dialog(this);
        button = (Button) findViewById(R.id.next2);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TextView userName = (TextView)findViewById(R.id.username); //text view to object
                String regexStr = "^[a-z0-9_]{6,12}$";
                if(userName.getText().toString().matches(regexStr)==false ){
                    ShowPopup();
                }
                else {
                    openActivity3();
                }
            }
        });
    }
    public void ShowPopup (){
        TextView txtclose;
        myDialog.setContentView(R.layout.popupwrongusername);
        txtclose = (TextView) myDialog.findViewById(R.id.popup2);
        txtclose.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                myDialog.dismiss();
            }
        });
        myDialog.show();

    }
    /** Called when the user taps the Send button */
    public void openActivity3() {
        Intent intent = new Intent(this, Activity3.class);
        startActivity(intent);
    }
}
